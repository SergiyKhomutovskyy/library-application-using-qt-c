#ifndef LIBIMPROVED_H
#define LIBIMPROVED_H

#include <QWidget>
#include "QStandardItemModel"
#include "QStandardItem"
#include <QtCore>
#include <QtGui>
#include "myproxy.h"

namespace Ui {
class LibImproved;
}

class LibImproved : public QWidget
{
    Q_OBJECT

public:
    explicit LibImproved(QWidget *parent = nullptr);
    ~LibImproved();

    QString Authorfilter() const;
    QString Titlefilter() const;
    QString Yearfilter() const;

    public slots:
    void AddBookButtonClicked();
    void EditBookButtonClicked();
    void Delete();
    //void doEdit();



private slots:
    void on_DisplayList_clicked();

    void on_DeleteButton_clicked();



    void on_FilterButton_clicked();

    void on_ClearButton_clicked();

private:
    Ui::LibImproved *ui;

    QStandardItemModel *mymodel = new QStandardItemModel;

    MyProxy *Myproxyobj = new MyProxy;
    QStandardItem *myitem;
};

#endif // LIBIMPROVED_H
