#ifndef MYPROXY_H
#define MYPROXY_H

#include <QMainWindow>
#include <QObject>
#include <QWidget>
#include "QSortFilterProxyModel"

class MyProxy : public QSortFilterProxyModel
{
    Q_OBJECT

    QString Author;
    QString Title;
    QString Year;

public:
    MyProxy(QObject *parent = 0); //constructor


    void setter(QString AuthorfromFilter,QString TitleFromFilte,QString YearFromFilter);
    bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const;

};
#endif // MYPROXY_H
