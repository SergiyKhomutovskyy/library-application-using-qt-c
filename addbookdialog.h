#ifndef ADDBOOKDIALOG_H
#define ADDBOOKDIALOG_H

//#include <QMainWindow>
//#include <QObject>
//#include <QWidget>
#include <QtGui>
#include <QDialog>
#include "QLineEdit"
#include "QSpinBox"

//class QLineEdit;
//class QSpinBox;
class AddBookDialog : public QDialog
{
public:
   AddBookDialog();

   //methods to return information provided in Lineedits
   QString getAuthor() const;
   QString getTitle() const;
   QString getYear() const;

   void setAuthor(QString );
   void setTitle(QString);
   void setYear(QString);

   private:
   QLineEdit *AuthorLine;
   QLineEdit *TitleLine;
   QLineEdit *YearLine;
};

#endif // ADDBOOKDIALOG_H
