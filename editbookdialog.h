#ifndef EDITBOOKDIALOG_H
#define EDITBOOKDIALOG_H

//#include <QMainWindow>
//#include <QObject>
//#include <QWidget>
#include <QtGui>
#include <QDialog>
#include "QLineEdit"
#include "QSpinBox"

//class QLineEdit;
//class QSpinBox;

class EditBookDialog : public QDialog
{
public:
   EditBookDialog();

   //methods to return information provided in Lineedits
   QString getAuthor() const;
   QString getTitle() const;
   QString getYear() const;

   void setAuthor(QString );
   void setTitle(QString);
   void setYear(QString);

   private:
   QLineEdit *AuthorLine;
   QLineEdit *TitleLine;
   QLineEdit *YearLine;
};
#endif // EDITBOOKDIALOG_H
