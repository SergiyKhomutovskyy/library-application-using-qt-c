#include "addbookdialog.h"
#include "QDialogButtonBox"
#include "QFormLayout"


AddBookDialog::AddBookDialog()
{

    QDialogButtonBox *buttonbox = new QDialogButtonBox;
    buttonbox->setStandardButtons(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    AuthorLine = new QLineEdit;
    TitleLine = new QLineEdit;
    YearLine = new QLineEdit;
    QFormLayout *layout = new QFormLayout(this);
    layout->addRow("Author:",AuthorLine);
    layout->addRow("Title:",TitleLine);
    layout->addRow("Year:",YearLine);
    layout->addWidget(buttonbox);

    // if Ok Button was clicked
    connect(buttonbox,SIGNAL(accepted()),this,SLOT(accept()));

    //if Cancel Button was clicked
    connect(buttonbox,SIGNAL(rejected()),this,SLOT(reject()));
}


//getting text from lines
QString AddBookDialog::getAuthor() const
{
    return AuthorLine->text();
}
QString AddBookDialog::getTitle() const
{
    return TitleLine->text();
}


QString AddBookDialog::getYear() const
{
    return YearLine->text();
}
