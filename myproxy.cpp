#include "myproxy.h"

MyProxy::MyProxy(QObject *parent)  : QSortFilterProxyModel(parent)
{

}

void MyProxy ::setter(QString AuthorfromFilter,QString TitleFromFilter,QString YearFromFilter)
{
    Author = AuthorfromFilter;
    Title = TitleFromFilter;
    Year = YearFromFilter;

    invalidateFilter();
}


bool MyProxy ::filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const
{

QModelIndex indexAuthor = sourceModel() -> index(sourceRow,0,sourceParent);
QModelIndex indexTitle = sourceModel() -> index(sourceRow,1,sourceParent);
QModelIndex indexYear = sourceModel() -> index(sourceRow,2,sourceParent);

if(sourceModel()->data(indexAuthor).toString().contains(Author)
        && sourceModel()->data(indexTitle).toString().contains(Title) &&
               sourceModel()->data(indexYear).toString().contains(Year))
    return true;
else
    return false;


}
