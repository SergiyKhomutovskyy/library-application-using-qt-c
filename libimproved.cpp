#include "libimproved.h"
#include "ui_libimproved.h"
#include "myproxy.h"
#include "addbookdialog.h"
#include "editbookdialog.h"
#include <QMessageBox>
#include <QTextStream>

LibImproved::LibImproved(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LibImproved)
{
       ui->setupUi(this);

        QStringList horizontalHeader;
        horizontalHeader.append("Author");
        horizontalHeader.append("Title");
        horizontalHeader.append("Year");

        mymodel->setHorizontalHeaderLabels(horizontalHeader);

        ui->tableView->setModel(mymodel);

        //to extend size to entire window
        ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

        //to select whole row with one click
        ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);

        //initializing proxy model,for being able to filter data
        ui->tableView->setModel(Myproxyobj);
        Myproxyobj->setSourceModel(mymodel);

        //add book button connect
        connect(ui->AddBook, SIGNAL(clicked()), this, SLOT( AddBookButtonClicked()));

        //edit book button connect
        connect(ui->EditButton,SIGNAL(clicked()),this, SLOT( EditBookButtonClicked()));



}

LibImproved::~LibImproved()
{
    delete ui;
}

void LibImproved::AddBookButtonClicked()
{
    AddBookDialog newDialog;
    int result = newDialog.exec();

    //if cancel was clicked
    if (result == AddBookDialog::Rejected)
        return;

    QString Author = newDialog.getAuthor();
    QString Title = newDialog.getTitle();
    QString Year = newDialog.getYear();

    //if nothing was provided i want OK to quit the bookdescription window
    if(newDialog.getAuthor().isEmpty() && newDialog.getTitle().isEmpty()
            && newDialog.getYear().isEmpty())
    {
        if(result == AddBookDialog::Accepted)
            return;
    }


    //Inserting in The table
    mymodel->insertRow(mymodel->rowCount());

    int row = mymodel->rowCount()-1;
    myitem = new QStandardItem(Author);
    mymodel->setItem(row,0,myitem);

    myitem = new QStandardItem(Title);
    mymodel->setItem(row,1,myitem);

    myitem = new QStandardItem(Year);
    mymodel->setItem(row,2,myitem);

    //Saving to the File


    QFile myfile("D:\\Documents\\education process\\EGUI\\LabsMine\\Lab1\\QtLibrary\\LibImprovedEgui\\library.txt");
    if(!myfile.open(QIODevice::Append | QIODevice::Text))
        return;

    QTextStream out(&myfile);
    out << Author << "|" << Title << "|" << Year << "\n";
    myfile.close();
}





void LibImproved::on_DisplayList_clicked()
{
    Delete();  //to "update" the list
    Myproxyobj->setter("","","");
    //Myproxyobj->setter("","","");
    QFile file("D:\\Documents\\education process\\EGUI\\LabsMine\\Lab1\\QtLibrary\\LibImprovedEgui\\library.txt");
        if(!file.open(QIODevice::ReadOnly |QIODevice::Text )) {
            QMessageBox::information(nullptr, "error", file.errorString());
        }

        QTextStream in(&file);

            while(!in.atEnd()) {
            QString line = in.readLine();
            QStringList fields = line.split("|");
            QString Author = fields[0];
            QString Title = fields[1];
            QString Year = fields[2];

            mymodel->insertRow(mymodel->rowCount());

            int row = mymodel->rowCount()-1;

            myitem = new QStandardItem(Author);
            mymodel->setItem(row,0,myitem);

            myitem = new QStandardItem(Title);
            mymodel->setItem(row,1,myitem);

            myitem = new QStandardItem(Year);
            mymodel->setItem(row,2,myitem);


        }
       // ui->tableWidget->resizeRowsToContents();
        //ui->tableWidget->resizeColumnsToContents();
        file.close();

}


//this method is to be used in Displaybutton,to kind of update the list
//not related with any button
void LibImproved::Delete()
{
    while(mymodel->rowCount() > 0)
    {
        mymodel->removeRow(0);
    }
}


void LibImproved::EditBookButtonClicked()
{

        //Firstly lets check whether something is selected or not
        QItemSelectionModel *MyCurrentSelectionModel = ui->tableView->selectionModel();

        //connect(MyCurrentSelectionModel,SIGNAL(&QItemSelectionModel::hasSelection()),this,SLOT(doEdit()));
        if(MyCurrentSelectionModel->hasSelection())
               {
                //if something is selected
                 //QModelIndexList IndexList = ui->tableView->selectionModel()->selection()
                //How Many rows were selected?I assume we can edit one book at atime
                QModelIndexList HowManyRowsList = MyCurrentSelectionModel->selectedRows();


                if(HowManyRowsList.count() == 1)
                {
                    //window responsible for editing the book
                    EditBookDialog EditBookDialog;
                    EditBookDialog.setWindowTitle("Edit Description of the Book");

                    int result = EditBookDialog.exec();

                    //if cancel was clicked
                       if (result == EditBookDialog::Rejected)
                           return;


                    QString Author =  EditBookDialog.getAuthor();
                    QString Title =  EditBookDialog.getTitle();
                    QString Year =  EditBookDialog.getYear();

                    //if nothing was provided i want OK to quit the bookdescription window
                    if(EditBookDialog.getAuthor().isEmpty() && EditBookDialog.getTitle().isEmpty()
                            && EditBookDialog.getYear().isEmpty())
                    {
                        if(result == EditBookDialog::Accepted)
                            return;
                    }


                    auto idx = HowManyRowsList.first();
                    idx = Myproxyobj->mapToSource(idx);
                    int row = idx.row();

                    myitem = new QStandardItem(Author);
                    mymodel->setItem(row,0,myitem);

                    myitem = new QStandardItem(Title);
                    mymodel->setItem(row,1,myitem);

                    myitem = new QStandardItem( Year);
                    mymodel->setItem(row,2,myitem);




                  //Store the table without the deleted row to the file
                  QFile mytxtfile("D:\\Documents\\education process\\EGUI\\LabsMine\\Lab1\\QtLibrary\\LibImprovedEgui\\library.txt");

                  if(!mytxtfile.open(QIODevice::Append |QIODevice::Truncate |QIODevice::Text))
                      return;

                      QTextStream out(&mytxtfile);


                      for(int i = 0; i<mymodel->rowCount(); i++)
                      out << mymodel->item(i,0)->text() << "|" << mymodel->item(i,1)->text() << "|" << mymodel->item(i,2)->text() << "\n";

                      mytxtfile.close();
                }

                else {
                    QMessageBox somemessage;
                    somemessage.setWindowTitle("Info:)");
                    somemessage.setText("Please Select ONE Book");
                    somemessage.exec();
                    return;
                }

                }
                else
                {
                    QMessageBox somemessage;
                    somemessage.setWindowTitle("Info:)");
                    somemessage.setText("Please Select the book to be editted");
                    somemessage.exec();
                      return;
                }


 }


void LibImproved::on_DeleteButton_clicked()
{
    //Firstly lets check whether something is selected or not
    QItemSelectionModel *select = ui->tableView->selectionModel();


    if(select->hasSelection())
    {
    //if something is selected

    QModelIndexList ListOfIndexes = select->selectedRows();
    int HowManyRowsToBeDeleted = ListOfIndexes.count();

    if(HowManyRowsToBeDeleted == 1)
    {

       int row =  ListOfIndexes.at(0).row();
      Myproxyobj->removeRow(row);


      //Store the table without the deleted row to the file
      QFile mytxtfile("D:\\Documents\\education process\\EGUI\\LabsMine\\Lab1\\QtLibrary\\LibImprovedEgui\\library.txt");
      if(!mytxtfile.open(QIODevice::Append |QIODevice::Truncate |QIODevice::Text))
          return;

          QTextStream out(&mytxtfile);


          for(int i = 0; i<mymodel->rowCount(); i++)
          out << mymodel->item(i,0)->text() << "|" << mymodel->item(i,1)->text() << "|" << mymodel->item(i,2)->text() << "\n";

          mytxtfile.close();
    }

    else {
        QMessageBox somemessage;
        somemessage.setWindowTitle("Info:)");
        somemessage.setText("Please Select ONE Book");
        somemessage.exec();
        return;
    }
    }

  else {
        //nothing is selected
        //app crashes without this check :/

        QMessageBox somemessage;
        somemessage.setWindowTitle("Info:)");
        somemessage.setText("Please Select a Book to be Deleted");
        somemessage.exec();
        return;
    }

}

QString LibImproved:: Authorfilter() const
{
    return ui->AuthorFIlterLine->text();

}

QString LibImproved:: Titlefilter() const
{

   return ui->TitleFilterLine->text();
}

QString LibImproved:: Yearfilter() const
{

    return ui->YearFilterLine->text();
}


void LibImproved::on_FilterButton_clicked()
{
    MyProxy MyProxyObject(this);
    QString Author = Authorfilter();
    QString Title = Titlefilter();
    QString Year = Yearfilter();
    Myproxyobj->setter(Author,Title,Year);
}

void LibImproved::on_ClearButton_clicked()
{
    Myproxyobj->setter("","","");
}
