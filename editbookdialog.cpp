#include "editbookdialog.h"
#include "QDialogButtonBox"
#include "QFormLayout"

EditBookDialog::EditBookDialog()
{

    QDialogButtonBox *buttonbox = new QDialogButtonBox;
    buttonbox->setStandardButtons(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    AuthorLine = new QLineEdit;
    TitleLine = new QLineEdit;
    YearLine = new QLineEdit;
    QFormLayout *layout = new QFormLayout(this);
    layout->addRow("Author:",AuthorLine);
    layout->addRow("Title:",TitleLine);
    layout->addRow("Year:",YearLine);
    layout->addWidget(buttonbox);

    // if Ok Button was clicked
    connect(buttonbox,SIGNAL(accepted()),this,SLOT(accept()));

    //if Cancel Button was clicked
    connect(buttonbox,SIGNAL(rejected()),this,SLOT(reject()));
}


//getting text from lines

QString EditBookDialog::getAuthor() const
{
    return AuthorLine->text();
}
QString EditBookDialog::getTitle() const
{
    return TitleLine->text();
}


QString EditBookDialog::getYear() const
{
    return YearLine->text();
}
